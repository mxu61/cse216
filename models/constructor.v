module constructor {
	type state_status_t = enum {IDLE, READ_ALPHABET, MINHEAPIFY, BUILD_HUFFMAN};
	type sub_state_status_t = enum {SUB_IDLE, EXTRACT_MIN_AS_LEFT, EXTRACT_MIN_AS_RIGHT, NEW_NODE_MINHEAPIFY};

	// const - parameter
	// TODO: add assumption for it
	// assume (ALPHABET_START_ADDR == 1bv8)
	const ALPHABET_START_ADDR: common.addr_t;
	// assume (ALPHABET_SIZE == 95bv8)
	const ALPHABET_SIZE: common.addr_t;
	// inputs
	// remove for now..?
	// start with READ_ALPHABET
	// input start: boolean;

	// feel that eventually need to seperate
	// them into different array
	// type node_memory_entry_t = 
	// record {char: char_t, freq: freq_t,
	// left_addr: addr_t, right_addr: addr_t};
	// input r_node_1: common.node_memory_entry_t;
	// input r_node_2: common.node_memory_entry_t;
	// input r_node_3: common.node_memory_entry_t;

	// outpurs
	output done: boolean;
	output huffman_root: common.addr_t;
	// output w_new_node_entry: common.node_memory_entry_t;

	// TODO, when to write
	// output reg [7:0] w_node_character,
	// output reg [ADDR_SIZE-1:0] w_left_child_addr,
	// output reg [ADDR_SIZE-1:0] w_right_child_addr,
	// output reg [FREQ_SIZE-1:0] w_freq,
	var valid: boolean;

	var w_node_character: common.char_t;
	var w_left_child_addr: common.addr_t;
	var w_right_child_addr: common.addr_t;
	var w_freq: common.freq_t;


	// type node_memory_entry_t = 
	// record {char: char_t, freq: freq_t,
	// left_addr: addr_t, right_addr: addr_t};
	var char_mem: [common.addr_t]common.addr_t;
	var freq_mem: [common.addr_t]common.freq_t;
	var left_mem: [common.addr_t]common.addr_t;
	var right_mem: [common.addr_t]common.addr_t;

	// // actually wire
	// output reg [ADDR_SIZE-1:0] r_node_addr_1,
	// output wire [ADDR_SIZE-1:0] r_node_addr_2,
	// output wire [ADDR_SIZE-1:0] r_node_addr_3,
	// output reg [ADDR_SIZE-1:0] w_node_addr,
	var r_node_addr_1: common.addr_t;
	var r_node_addr_2: common.addr_t;
	var r_node_addr_3: common.addr_t;
	var w_node_addr: common.addr_t;

	// internals
	var	state: state_status_t;
	var caller_state: state_status_t;
	var sub_state: sub_state_status_t;

	// the size of the heap is max_node
	// whose index is of type addr_t
	var heap_array: [common.addr_t]common.addr_t;

	var heap_index: common.addr_t;
	var left_child_heap_index: common.addr_t;
	var right_child_heap_index: common.addr_t;

	var heap_size: common.addr_t;

	var new_node_addr_counter: common.addr_t;
	var alphabet_addr_counter: common.addr_t;

	// minheapifying the whole heap needs
	// a loop counter
	var minheapify_loop_counter: common.addr_t;

	// the comparisions
	var left_bound, right_bound: boolean;
	var less1, less2, less3: boolean;

	var smallest_heap_index: common.addr_t;

	// INIT
	init {
		// state = IDLE;
		state = READ_ALPHABET;

		// new_node_addr_counter = 0bv8;
		new_node_addr_counter = ALPHABET_START_ADDR + ALPHABET_SIZE;
			
		sub_state = SUB_IDLE;
		caller_state = IDLE;

		valid = false;

		heap_index = 0bv8;
		heap_size = 0bv8;

		minheapify_loop_counter = 0bv8;

		alphabet_addr_counter = 1bv8;

		// the $ sign
		// record {char: char_t, freq: freq_t,
		// left_addr: addr_t, right_addr: addr_t};
	
		// w_new_node_entry.char_t = 36bv8;
		// w_new_node_entry.left_addr = 0bv8;
		// w_new_node_entry.right_addr = 0bv8;
		// w_new_node_entry.freq = 0bv16;
		w_node_character = 36bv8;
		w_left_child_addr = 0bv8;
		w_right_child_addr = 0bv8;
		w_freq = 0bv16;

		// initialize mem
		// TODO: the transition from init to sencodn state
		// and the start signal

		// the max node loop
        for i in range (0bv8, 255bv8) {
			heap_array[i] = 0bv8;
        }

		done = false;
		huffman_root = 0bv8;
	}

	procedure write_mem()
		modifies char_mem, left_mem, right_mem, freq_mem;
	{
		char_mem[w_node_addr] = w_node_character;
		left_mem[w_node_addr] = w_left_child_addr;
		right_mem[w_node_addr] = w_right_child_addr;
		freq_mem[w_node_addr] = w_freq;
	}

	procedure get_smallest(size_param: common.addr_t, p_index_param: common.addr_t,
						l_index_param: common.addr_t, r_index_param: common.addr_t,
						p_freq_param: common.freq_t,
						l_freq_param: common.freq_t, r_freq_param: common.freq_t)
		returns (result_index: common.addr_t)
	{
		// var result_index: common.addr_t;
		var result_freq: common.freq_t;

		result_index = p_index_param;
		result_freq = p_freq_param;

		if (l_index_param < size_param && l_freq_param < result_freq) {
			result_index = l_index_param;
			result_freq = l_freq_param;
		}

		if (r_index_param < size_param && r_freq_param < result_freq) {
			result_index = r_index_param;
			result_freq = r_freq_param;
		}
	}

	// the FSM
	// all state separated
	procedure state_machine()
		modifies heap_array, heap_index, state, caller_state, heap_size,
		minheapify_loop_counter, alphabet_addr_counter, sub_state,
		w_freq, w_left_child_addr, w_right_child_addr, new_node_addr_counter,
		valid, w_node_addr, done, huffman_root;
	{
		if (state == READ_ALPHABET) {
			// TODO: all node info need to switch to internal mem
			if (freq_mem[r_node_addr_1] != 0bv16) {
				heap_array[heap_index] = alphabet_addr_counter;
				if (alphabet_addr_counter != ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv8) {
					heap_index = heap_index + 1bv8;
				}
			}

			// will the tool try to share wires?
			// TODO the new start vs. this sum
			if (alphabet_addr_counter == ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv8) {
				// minheapify the whole array
				state = MINHEAPIFY;
				caller_state = READ_ALPHABET;
				if (freq_mem[r_node_addr_1] != 0bv16) { 
					heap_size = heap_index + 1bv8;
					// the minheapify index loaded!!!
					// heap_index <= (heap_index - 1) >> 1;
					// minheapify_loop_counter <= (heap_index - 1) >> 1;
					heap_index = (heap_index - 1bv8) / 2bv8;
					minheapify_loop_counter = (heap_index - 1bv8) / 2bv8;
				} else {
					heap_size = heap_index;
					// the minheapify index loaded!!!
					// heap_index <= (heap_index - 2) >> 1;
					// minheapify_loop_counter <= (heap_index - 2) >> 1;
					heap_index = (heap_index - 2bv8) / 2bv8;
					minheapify_loop_counter = (heap_index - 2bv8) / 2bv8;
				}
			} else {
				alphabet_addr_counter = alphabet_addr_counter + 1bv8;
			}
		}

		if (state == MINHEAPIFY) {
			if (smallest_heap_index != heap_index) {
				// swap smallest
				heap_array[smallest_heap_index] = heap_array[heap_index];
				heap_array[heap_index] = heap_array[smallest_heap_index];
				// update heap index for recursive call
				heap_index = smallest_heap_index;
				// dont need to do anything
				// the fsm repeats itself
				// say thank you fsm
			 } else {
				// decide what's next based on caller state
				if (caller_state == READ_ALPHABET) {
					// keep looping until zero index
					if (minheapify_loop_counter == 0bv8) {
						state = BUILD_HUFFMAN;
						sub_state = EXTRACT_MIN_AS_LEFT;
						// to read min
						// it shall be zero anyway
						heap_index = 0bv8;
					} else {
						heap_index = minheapify_loop_counter - 1bv8;
						minheapify_loop_counter = minheapify_loop_counter - 1bv8;
					}
				} else /*if (caller_state == BUILD_HUFFMAN) TODO: is this okay*/{
					state = caller_state;
					// for extract min
					heap_index = 0bv8;
			 	}
			}
		}

		if (state == BUILD_HUFFMAN) {
			// the opposite: heap_size == 1 && sub_state == EXTRACT_MIN_AS_LEFT
			if (heap_size != 1bv8 || sub_state != EXTRACT_MIN_AS_LEFT) {
				if (sub_state == EXTRACT_MIN_AS_LEFT) {
					w_freq = freq_mem[r_node_addr_1];
					w_left_child_addr = heap_array[heap_index];
					// swap
					// heap_index shall be zero
					heap_array[heap_index] = heap_array[heap_size-1bv8];
					heap_size = heap_size - 1bv8;

					// minheapify
					// make sure the index is zero
					state = MINHEAPIFY;
					caller_state = BUILD_HUFFMAN;

					sub_state = EXTRACT_MIN_AS_RIGHT;
				}
				
				if (sub_state == EXTRACT_MIN_AS_RIGHT) {
					w_freq = w_freq + freq_mem[r_node_addr_1];
					w_right_child_addr = heap_array[heap_index];

					// swap
					// heap_index shall be zero
					heap_array[heap_index] = new_node_addr_counter;
					new_node_addr_counter = new_node_addr_counter + 1bv8;

					// delay minheapify
					// such that the write can finishe first

					// write to node memory
					// TODO make it internal
					valid = true;
					w_node_addr = new_node_addr_counter;
					
					sub_state = NEW_NODE_MINHEAPIFY;
				}
				
				if (sub_state == NEW_NODE_MINHEAPIFY) {
					// disable write
					valid = false;

					// minheapify
					state = MINHEAPIFY;
					caller_state = BUILD_HUFFMAN;

					// looooooooooooop
					// it stops or not depending on the heap_size
					sub_state = EXTRACT_MIN_AS_LEFT;
				}
			} else {
				//done
				state = IDLE;
				done = true;
				huffman_root = heap_array[0bv8];
			}
		}
	}

	// NEXT
	next {
		// TODO:
		// diff between combi logic and sequan logic?
		// once set data, will those combi update?
		// --> TODO, depend on new value or old value, the ''!!!
		// to model input, need to write a upper model that interact and call next 

		// those stable connections --and-- those combilogic or say wire
		// assign r_node_addr_1 = (state == READ_ALPHABET) ? alphabet_addr_counter : heap_array[heap_index];
		if (state' == READ_ALPHABET) {
			r_node_addr_1' = alphabet_addr_counter';
		} else {
			r_node_addr_1' = heap_array'[heap_index'];
		}

		// the left child index wrt. the cur index
		// wire [ADDR_SIZE-1:0] left_child_heap_index = (heap_index << 1) + 1;
		// assign r_node_addr_2 = heap_array[left_child_heap_index];
		// TODO, feel that static connections should use '
		left_child_heap_index' = (heap_index' * 2bv8) + 1bv8;
		r_node_addr_2' = heap_array'[left_child_heap_index'];

		// the right child index wrt. the cur index
		// wire [ADDR_SIZE-1:0] right_child_heap_index = (heap_index << 1) + 2;
		// assign r_node_addr_3 = heap_array[right_child_heap_index];
		right_child_heap_index' = (heap_index' * 2bv8) + 2bv8;
		r_node_addr_3' = heap_array'[right_child_heap_index'];


		// the comparision logic
		// not sure if this is legal...
		// left_bound' = left_child_heap_index' < heap_size';
		// right_bound' = right_child_heap_index' < heap_size';

		// left vs. cur, cur vs. right, left vs. right
		// assign less1 = r_freq_2 < r_freq_1;
		// assign less2 = r_freq_1 < r_freq_3;
		// assign less3 = r_freq_2 < r_freq_3;
		// TODO synthesis truth table?

		// input r_node_1: common.node_memory_entry_t;
		//     type node_memory_entry_t = 
		// record {char: char_t, freq: freq_t,
		// left_addr: addr_t, right_addr: addr_t};
		// TODO: the placement of '
		// TODO: the new value of input? do i need the '
		// TODO: just as internal mem, modify it all the way you want
		
		// TODO: need to transform into internal mem
		// less1' = r_node_2.freq_t' < r_node_1.freq_t';
		// less2' = r_node_1.freq_t' < r_node_3.freq_t';
		// less3' = r_node_2.freq_t' < r_node_3.freq_t';
		// less1' = freq_mem'[r_node_addr_2] < freq_mem'[r_node_addr_1];
		// less2' = freq_mem'[r_node_addr_1] < freq_mem'[r_node_addr_3];
		// less3' = freq_mem'[r_node_addr_2] < freq_mem'[r_node_addr_3];

		// call update_smallest(size_param: common.addr_t, p_index_param: common.addr_t,
		// 					l_index_param: common.addr_t, r_index_param: common.addr_t,
		// 					p_freq_param: common.freq_t,
		// 					l_freq_param: common.freq_t, r_freq_param: common.freq_t)

		call (smallest_heap_index')
			= get_smallest(heap_size', heap_index',
							left_child_heap_index', right_child_heap_index',
							freq_mem'[r_node_addr_1'],
							freq_mem'[r_node_addr_2'], freq_mem'[r_node_addr_3']);

		// TODO check all '
		// no if else within case statements
		
		/*
		case
			(left_bound' == false && right_bound' == false): {
				smallest_heap_index' = heap_index';
			}
			(left_bound' == false && right_bound' == true): {
				if (less2') {
					smallest_heap_index' = heap_index';
				} else {
					smallest_heap_index' = right_child_heap_index';
				}
			}
			(left_bound' == true && right_bound' == false): {
				if (less1') {
					smallest_heap_index' = left_child_heap_index';
				} else {
					smallest_heap_index' = heap_index';
				}
			}
			(left_bound' == true && right_bound' == true): {
				case
					// {3'b000}:
					(less1' == false && less2' == false && less3' == false):
						smallest_heap_index' = right_child_heap_index';
					// {3'b01?}:
					// TODO is it legal
					(less1' == false && less2' == true):
						smallest_heap_index' = heap_index';
					// {3'b100}:
					(less1' == true && less2' == false && less3' == false):
						smallest_heap_index' = right_child_heap_index';
					// {3'b101}:
					(less1' == true && less2' == false && less3' == true):
						smallest_heap_index' = left_child_heap_index';
					// {3'b111}:
					(less1' == true && less2' == true && less3' == true):
						smallest_heap_index' = left_child_heap_index';
					default:
						smallest_heap_index' = 0bv8;
				esac
			}
		esac
		*/

		// no paralledl because of multiple update..
		// if with single state cannot skip {}?
		// TODO: simplify the compare logic
		// "prgrammingly"
		// and verify separatly / or relate to synthesis
		/*
		if (left_bound' == false && right_bound' == false) {
			smallest_heap_index' = heap_index';
		} else {
			if (left_bound' == false && right_bound' == true) {
				if (less2') {
					smallest_heap_index' = heap_index';
				}
				else {
					smallest_heap_index' = right_child_heap_index';
				}
			} else {
				if (left_bound' == true && right_bound' == false) {
					if (less1') {
						smallest_heap_index' = left_child_heap_index';
					} else {
						smallest_heap_index' = heap_index';
					}
				} else {
					if (left_bound' == true && right_bound' == true) {
						
						// {3'b000}:
						if (less1' == false && less2' == false && less3' == false) {
							smallest_heap_index' = right_child_heap_index';
						} else {
							// {3'b01?}:
							// TODO is it legal
							if (less1' == false && less2' == true) {
								smallest_heap_index' = heap_index';
							} else {
								// {3'b100}:
								if (less1' == true && less2' == false && less3' == false) {
									smallest_heap_index' = right_child_heap_index';
								} else {
									// {3'b101}:
									if (less1' == true && less2' == false && less3' == true) {
										smallest_heap_index' = left_child_heap_index';
									} else {
										// {3'b111}:
										if (less1' == true && less2' == true && less3' == true) {
											smallest_heap_index' = left_child_heap_index';
										} else {
										// default:
											smallest_heap_index' = 0bv8;
										}
									}
								}
							}
						}
					}
				}
			}
		}*/

		// the memory write
		// mem write happens only inside procudure lol why dont you tell me
		if (valid) {
			call write_mem();
		}

		call state_machine();
	} //end of init

}