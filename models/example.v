module test {
	const RANGE_START: bv4;
	const RANGE_SIZE: bv4;

	var mem: [bv4]bv4;

    init {
        assume(RANGE_START == 1bv4);
        assume(RANGE_SIZE == 8bv4);

		havoc mem;

        // in range [1bv4, 8bv4], 
		// there are at least two non zero elements in mem
		assume(exists (mix, miy: bv4):: 
			(mix >= RANGE_START) && (mix <= RANGE_START + RANGE_SIZE - 1bv4) &&
			(miy >= RANGE_START) && (miy <= RANGE_START + RANGE_SIZE - 1bv4) &&
			(mix != miy) && (mem[mix] != 0bv4) && (mem[miy] != 0bv4));
    }

    next {
    }

    // the invariant shall not hold
    invariant test_havoc: (false);

	control {
		vobj = bmc(6);
		check;
		vobj.print_cex();
	}
}