module constructor {
	type state_status_t = enum {IDLE, READ_ALPHABET, MINHEAPIFY, BUILD_HUFFMAN};
	type sub_state_status_t = enum {SUB_IDLE, EXTRACT_MIN_AS_LEFT, EXTRACT_MIN_AS_RIGHT, NEW_NODE_MINHEAPIFY};

	// const - parameter
	// TODO: add assumption for it
	// assume (ALPHABET_START_ADDR == 1bv4)
	// const ALPHABET_START_ADDR: common.addr_t;
	// assume (ALPHABET_SIZE == 8bv4)
	// const ALPHABET_SIZE: common.addr_t;
	// inputs

	// outpurs
	output done: boolean;
	output huffman_root: common.addr_t;
	// output w_new_node_entry: common.node_memory_entry_t;

	var ALPHABET_START_ADDR: common.addr_t;
	var ALPHABET_SIZE: common.addr_t;

	// TODO, when to write
	var valid: boolean;

	var w_node_character: common.char_t;
	var w_left_child_addr: common.addr_t;
	var w_right_child_addr: common.addr_t;
	var w_freq: common.freq_t;

	var char_mem: [common.addr_t]common.char_t;
	var freq_mem: [common.addr_t]common.freq_t;
	var left_mem: [common.addr_t]common.addr_t;
	var right_mem: [common.addr_t]common.addr_t;

	// // actually wire
	var r_node_addr_1: common.addr_t;
	var r_node_addr_2: common.addr_t;
	var r_node_addr_3: common.addr_t;
	var w_node_addr: common.addr_t;

	// internals
	var	state: state_status_t;
	var caller_state: state_status_t;
	var sub_state: sub_state_status_t;

	// the size of the heap is max_node
	// whose index is of type addr_t
	var heap_array: [common.addr_t]common.addr_t;

	var heap_index: common.addr_t;
	var left_child_heap_index: common.addr_t;
	var right_child_heap_index: common.addr_t;

	var heap_size: common.addr_t;

	var new_node_addr_counter: common.addr_t;
	var alphabet_addr_counter: common.addr_t;

	// minheapifying the whole heap needs
	// a loop counter
	var minheapify_loop_counter: common.addr_t;

	// the comparisions
	// var left_bound, right_bound: boolean;
	// var less1, less2, less3: boolean;

	var smallest_heap_index: common.addr_t;

	var clk_counter: integer;

	// INIT
	init {
		done = false;
		huffman_root = 0bv4;
		valid = false;

		ALPHABET_START_ADDR = 1bv4;
		ALPHABET_SIZE = 8bv4;

		w_node_character = 36bv8;
		w_left_child_addr = 0bv4;
		w_right_child_addr = 0bv4;
		w_freq = 0bv6;
		
		// ==============================================
		/* first way to initialize
		// initialize mem
		// zero addr is reserved
		char_mem[0bv4] = 0bv8;
		left_mem[0bv4] = 0bv4;
		right_mem[0bv4] = 0bv4;
		freq_mem[0bv4] = 0bv6;

		// alphabet
		// char duplication is not our concern
		for i in range (ALPHABET_START_ADDR, ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) {
			havoc char_mem[i];
			left_mem[i] = 0bv4;
			right_mem[i] = 0bv4;
			havoc freq_mem[i];
		}
		// at lest two charactors
		assume(exists (mix, miy: common.addr_t):: 
			(mix >= ALPHABET_START_ADDR) && (mix <= ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) &&
			(miy >= ALPHABET_START_ADDR) && (miy <= ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) &&
			(mix != miy) && (freq_mem[mix] != 0bv6) && (freq_mem[miy] != 0bv6));

		// new node space all zeros
		for i in range (ALPHABET_START_ADDR + ALPHABET_SIZE, 15bv4) {
			char_mem[i] = 0bv8;
			left_mem[i] = 0bv4;
			right_mem[i] = 0bv4;
			freq_mem[i] = 0bv6;
		}
		*/
		// ==============================================
		// second way to initialize

		// WHAT REALLY MATTERS:
		// inside alphabet, at least two non zero frequency
		// and all l/r are zeros

		// all left&right zeros
		for i in range (0bv4, 15bv4) {
			char_mem[i] = 0bv8;
			left_mem[i] = 0bv4;
			right_mem[i] = 0bv4;
		}

		// random char and freq
		// havoc char_mem;
		havoc freq_mem;

		// zero reserved but dont actually depend on it
		// assume(char_mem[0bv4] == 0bv8 && freq_mem[0bv4] == 0bv6);

		// for alphabet
		// at lest two charactors
		// without being specifc
		assume(exists (mix, miy: common.addr_t):: 
			(mix >=_u ALPHABET_START_ADDR) && (mix <=_u ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) &&
			(miy >=_u ALPHABET_START_ADDR) && (miy <=_u ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) &&
			(mix != miy) && (freq_mem[mix] != 0bv6) && (freq_mem[miy] != 0bv6));

		// being specific and less general
		// assume(freq_mem[1bv4] != 0bv6 && freq_mem[2bv4] != 0bv6);
		// assume(freq_mem[0bv4] == 0bv6);
		// assume(forall (fmi: common.addr_t)::
		// 		(fmi >= ALPHABET_START_ADDR + ALPHABET_SIZE) ==>
		// 		(freq_mem[fmi] == 0bv6));

		// for new node space
		// but actually dont care
		// assume(forall (mi: common.addr_t)::
		// 	(mi >= ALPHABET_START_ADDR + ALPHABET_SIZE && mi <= 15bv4) ==>
		// 	(char_mem[mi] == 0bv8 && freq_mem[mi] == 0bv6));

		// ==============================================
		// mem init for testing
		
		// for i in range (4bv4, 15bv4) {
		// 	char_mem[i] = 0bv8;
		// 	left_mem[i] = 0bv4;
		// 	right_mem[i] = 0bv4;
		// 	freq_mem[i] = 0bv6;
		// }

		// char_mem[1bv4] = 3bv8;
		// left_mem[1bv4] = 0bv4;
		// right_mem[1bv4] = 0bv4;
		// freq_mem[1bv4] = 3bv6;

		// char_mem[2bv4] = 2bv8;
		// left_mem[2bv4] = 0bv4;
		// right_mem[2bv4] = 0bv4;
		// freq_mem[2bv4] = 2bv6;

		// char_mem[3bv4] = 1bv8;
		// left_mem[3bv4] = 0bv4;
		// right_mem[3bv4] = 0bv4;
		// freq_mem[3bv4] = 1bv6;

		// ==============================================

		r_node_addr_1 = ALPHABET_START_ADDR;
		r_node_addr_2 = 0bv4;
		r_node_addr_3 = 0bv4;
		w_node_addr = 0bv4;

		// state = IDLE;
		state = READ_ALPHABET;
		sub_state = SUB_IDLE;
		caller_state = IDLE;

		// the max node loop
		for i in range (0bv4, 15bv4) {
			heap_array[i] = 0bv4;
		}

		heap_index = 0bv4;
		left_child_heap_index = 0bv4;
		right_child_heap_index = 0bv4;

		heap_size = 0bv4;

		// new_node_addr_counter = 0bv4;
		new_node_addr_counter = ALPHABET_START_ADDR + ALPHABET_SIZE;
		alphabet_addr_counter = ALPHABET_START_ADDR;
		minheapify_loop_counter = 0bv4;

		smallest_heap_index = 0bv4;
		clk_counter = 0;
		// TODO: the transition from init to sencodn state
		// and the start signal
	}

	procedure write_mem()
		modifies char_mem, left_mem, right_mem, freq_mem;
	{
		char_mem[w_node_addr] = w_node_character;
		left_mem[w_node_addr] = w_left_child_addr;
		right_mem[w_node_addr] = w_right_child_addr;
		freq_mem[w_node_addr] = w_freq;
	}

	procedure get_smallest(size_param: common.addr_t, p_index_param: common.addr_t,
						l_index_param: common.addr_t, r_index_param: common.addr_t,
						p_freq_param: common.freq_t,
						l_freq_param: common.freq_t, r_freq_param: common.freq_t)
		returns (result_index: common.addr_t)
	{
		// var result_index: common.addr_t;
		var result_freq: common.freq_t;

		result_index = p_index_param;
		result_freq = p_freq_param;

		if (l_index_param <_u size_param && l_freq_param <_u result_freq) {
			result_index = l_index_param;
			result_freq = l_freq_param;
		}

		if (r_index_param <_u size_param && r_freq_param <_u result_freq) {
			result_index = r_index_param;
			result_freq = r_freq_param;
		}
	}

	// the FSM
	// all state separated
	// fmTODO PROCEDURE has sequential semantics
	procedure state_machine()
		modifies heap_array, heap_index, state, caller_state, heap_size,
		minheapify_loop_counter, alphabet_addr_counter, sub_state,
		w_freq, w_left_child_addr, w_right_child_addr, new_node_addr_counter,
		valid, w_node_addr, done, huffman_root;
	{
		var tmp_addr: common.addr_t;
		var old_state: state_status_t;
		var old_sub_state: sub_state_status_t;

		old_state = state;
		old_sub_state = sub_state;

		if (old_state == READ_ALPHABET) {
			// TODO: all node info need to switch to internal mem
			if (freq_mem[r_node_addr_1] != 0bv6) {
				heap_array[heap_index] = alphabet_addr_counter;
				if (alphabet_addr_counter != ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) {
					heap_index = heap_index + 1bv4;
				}
			}

			// will the tool try to share wires?
			// TODO the new start vs. this sum
			if (alphabet_addr_counter == ALPHABET_START_ADDR + ALPHABET_SIZE - 1bv4) {
				// minheapify the whole array
				state = MINHEAPIFY;
				caller_state = READ_ALPHABET;
				if (freq_mem[r_node_addr_1] != 0bv6) { 
					heap_size = heap_index + 1bv4;
					// the minheapify index loaded!!!
					// heap_index <= (heap_index - 1) >> 1;
					// minheapify_loop_counter <= (heap_index - 1) >> 1;
					heap_index = (heap_index - 1bv4) /_u 2bv4;
					// minheapify_loop_counter = (heap_index - 1bv4) / 2bv4;
					minheapify_loop_counter = heap_index;
				} else {
					heap_size = heap_index;
					// the minheapify index loaded!!!
					// heap_index <= (heap_index - 2) >> 1;
					// minheapify_loop_counter <= (heap_index - 2) >> 1;
					heap_index = (heap_index - 2bv4) /_u 2bv4;
					// minheapify_loop_counter = (heap_index - 2bv4) / 2bv4;
					minheapify_loop_counter = heap_index;
				}
			} else {
				alphabet_addr_counter = alphabet_addr_counter + 1bv4;
			}
		}

		if (old_state == MINHEAPIFY) {
			if (smallest_heap_index != heap_index) {
				// swap smallest
				tmp_addr = heap_array[smallest_heap_index];
				heap_array[smallest_heap_index] = heap_array[heap_index];
				// heap_array[heap_index] = heap_array[smallest_heap_index];
				heap_array[heap_index] = tmp_addr;
				// update heap index for recursive call
				heap_index = smallest_heap_index;
				// dont need to do anything
				// the fsm repeats itself
				// say thank you fsm
			} else {
				// decide what's next based on caller state
				if (caller_state == READ_ALPHABET) {
					// keep looping until zero index
					if (minheapify_loop_counter == 0bv4) {
						state = BUILD_HUFFMAN;
						sub_state = EXTRACT_MIN_AS_LEFT;
						// to read min
						// it shall be zero anyway
						heap_index = 0bv4;
					} else {
						heap_index = minheapify_loop_counter - 1bv4;
						minheapify_loop_counter = minheapify_loop_counter - 1bv4;
					}
				} else /*if (caller_state == BUILD_HUFFMAN) TODO: is this okay*/{
					state = caller_state;
					// for extract min
					heap_index = 0bv4;
			 	}
			}
		}

		if (old_state == BUILD_HUFFMAN) {
			// the opposite: heap_size == 1 && sub_state == EXTRACT_MIN_AS_LEFT
			if (heap_size != 1bv4 || old_sub_state != EXTRACT_MIN_AS_LEFT) {
				if (old_sub_state == EXTRACT_MIN_AS_LEFT) {
					w_freq = freq_mem[r_node_addr_1];

					// Ast
					assert(forall (hi: common.addr_t):: ( hi >_u 0bv4 && hi <_u heap_size) ==> (freq_mem[r_node_addr_1] <=_u freq_mem[heap_array[hi]]));
					// assert(forall (hi: common.addr_t):: ( hi > 0bv4 && hi < heap_size) ==> (freq_mem[r_node_addr_1] == freq_mem[heap_array[hi]]));
					// assert(false);

					w_left_child_addr = heap_array[heap_index];
					// swap
					// heap_index shall be zero
					heap_array[heap_index] = heap_array[heap_size-1bv4];
					heap_size = heap_size - 1bv4;

					// minheapify
					// make sure the index is zero
					state = MINHEAPIFY;
					caller_state = BUILD_HUFFMAN;

					sub_state = EXTRACT_MIN_AS_RIGHT;
				}
				
				if (old_sub_state == EXTRACT_MIN_AS_RIGHT) {
					w_freq = w_freq + freq_mem[r_node_addr_1];

					// Ast
					assert(forall (hi: common.addr_t):: ( hi >_u 0bv4 && hi <_u heap_size) ==> (freq_mem[r_node_addr_1] <=_u freq_mem[heap_array[hi]]));

					w_right_child_addr = heap_array[heap_index];

					// swap
					// heap_index shall be zero
					heap_array[heap_index] = new_node_addr_counter;

					// write to node memory
					// TODO make it internal
					valid = true;
					w_node_addr = new_node_addr_counter;

					new_node_addr_counter = new_node_addr_counter + 1bv4;

					// delay minheapify
					// such that the write can finishe first
					
					sub_state = NEW_NODE_MINHEAPIFY;
				}
				
				if (old_sub_state == NEW_NODE_MINHEAPIFY) {
					// disable write
					valid = false;

					// minheapify
					state = MINHEAPIFY;
					caller_state = BUILD_HUFFMAN;

					// looooooooooooop
					// it stops or not depending on the heap_size
					sub_state = EXTRACT_MIN_AS_LEFT;
				}
			} else {
				//done
				state = IDLE;
				done = true;
				huffman_root = heap_array[0bv4];
			}
		}
	}

	// NEXT
	next {
		clk_counter' = clk_counter + 1;
		// TODO:
		// diff between combi logic and sequan logic?
		// once set data, will those combi update?
		// --> TODO, depend on new value or old value, the ''!!!
		// to model input, need to write a upper model that interact and call next 

		// those stable connections --and-- those combilogic or say wire
		// assign r_node_addr_1 = (state == READ_ALPHABET) ? alphabet_addr_counter : heap_array[heap_index];
		if (state' == READ_ALPHABET) {
			r_node_addr_1' = alphabet_addr_counter';
		} else {
			r_node_addr_1' = heap_array'[heap_index'];
		}

		// the left child index wrt. the cur index
		// wire [ADDR_SIZE-1:0] left_child_heap_index = (heap_index << 1) + 1;
		// assign r_node_addr_2 = heap_array[left_child_heap_index];
		// TODO, feel that static connections should use '
		left_child_heap_index' = (heap_index' * 2bv4) + 1bv4;
		r_node_addr_2' = heap_array'[left_child_heap_index'];

		// the right child index wrt. the cur index
		// wire [ADDR_SIZE-1:0] right_child_heap_index = (heap_index << 1) + 2;
		// assign r_node_addr_3 = heap_array[right_child_heap_index];
		right_child_heap_index' = (heap_index' * 2bv4) + 2bv4;
		r_node_addr_3' = heap_array'[right_child_heap_index'];

		call (smallest_heap_index')
			= get_smallest(heap_size', heap_index',
							left_child_heap_index', right_child_heap_index',
							freq_mem'[r_node_addr_1'],
							freq_mem'[r_node_addr_2'], freq_mem'[r_node_addr_3']);

		if (valid) {
			call write_mem();
		}

		call state_machine();

		/*case
			(clk_counter == 1): {
				// assert(heap_index == 1bv4 && alphabet_addr_counter == 2bv4);
				assert(false);
			}
			(clk_counter ==2): {
				assert(false);
			}
		esac*/
	}

	// invariant test_clk2_state: ((clk_counter == 2) ==> (state == READ_ALPHABET));
	// invariant test_clk2_index: ((clk_counter == 2) ==> (heap_index == 2bv4));
	// invariant test_clk2_alphabet_index: ((clk_counter == 2) ==> (alphabet_addr_counter == 3bv4));
	// invariant test_clk2_freq: ((clk_counter == 2) ==> (freq_mem[alphabet_addr_counter - 1bv4] == 2bv6));

	// invariant test_clk3_state: ((clk_counter == 3) ==> (state == READ_ALPHABET));
	// invariant test_clk3_index: ((clk_counter == 3) ==> (heap_index == 3bv4));
	// invariant test_clk3_alphabet_index: ((clk_counter == 3) ==> (alphabet_addr_counter == 4bv4));
	// invariant test_clk3_freq: ((clk_counter == 3) ==> (freq_mem[alphabet_addr_counter - 1bv4] == 1bv6));

	// invariant test_clk4_state: ((clk_counter == 4) ==> (state == READ_ALPHABET));
	// invariant test_clk4_index: ((clk_counter == 4) ==> (heap_index == 3bv4));
	// invariant test_clk4_alphabet_index: ((clk_counter == 4) ==> (alphabet_addr_counter == 5bv4));
	// invariant test_clk4_freq: ((clk_counter == 4) ==> (freq_mem[alphabet_addr_counter - 1bv4] == 0bv6));

	// invariant test_clk7_state: ((clk_counter == 7) ==> (state == READ_ALPHABET));
	// invariant test_clk7_index: ((clk_counter == 7) ==> (heap_index == 3bv4));
	// invariant test_clk7_alphabet_index: ((clk_counter == 7) ==> (alphabet_addr_counter == 8bv4));
	// invariant test_clk7_freq: ((clk_counter == 7) ==> (freq_mem[alphabet_addr_counter - 1bv4] == 0bv6));

	// // transit
	// invariant test_clk8_state: ((clk_counter == 8) ==> (state == MINHEAPIFY));
	// invariant test_clk8_caller_state: ((clk_counter == 8) ==> (caller_state == READ_ALPHABET));
	// invariant test_clk8_size: ((clk_counter == 8) ==> (heap_size == 3bv4));
	// invariant test_clk8_index: ((clk_counter == 8) ==> (heap_index == 0bv4));
	// invariant test_clk8_loop_counter: ((clk_counter == 8) ==> (minheapify_loop_counter == 0bv4));
	// invariant test_clk8_alphabet_index: ((clk_counter == 8) ==> (alphabet_addr_counter == 8bv4));
	// invariant test_clk8_freq: ((clk_counter == 8) ==> (freq_mem[alphabet_addr_counter] == 0bv6));
	// // r_node_addr_1' = heap_array'[heap_index']
	// invariant test_clk8_read_addr: ((clk_counter == 8) ==> (r_node_addr_1 == 1bv4));
	// // (heap_index - 2bv4) / 2bv4;
	// invariant test_clk8_sml_index: ((clk_counter == 8) ==> (smallest_heap_index == 2bv4));

	// minheapified
	// invariant test_clk9_state: ((clk_counter == 9) ==> (state == MINHEAPIFY));
	// invariant test_clk9_index: ((clk_counter == 9) ==> (heap_index == 2bv4));
	// invariant test_clk9_sml_index: ((clk_counter == 9) ==> (smallest_heap_index == 2bv4));

	// transited to build
	// state = BUILD_HUFFMAN;
	// sub_state = EXTRACT_MIN_AS_LEFT;
	// heap_index = 0bv4;
	// invariant test_clk10_state: ((clk_counter == 10) ==> (state == BUILD_HUFFMAN));
	// invariant test_clk10_sub_state: ((clk_counter == 10) ==> (sub_state == EXTRACT_MIN_AS_LEFT));
	// invariant test_clk10_index: ((clk_counter == 10) ==> (heap_index == 0bv4));

	// extracted left
	// w_freq = freq_mem[r_node_addr_1];
	// w_left_child_addr = heap_array[heap_index];
	// // swap
	// // heap_index shall be zero
	// heap_array[heap_index] = heap_array[heap_size-1bv4];
	// heap_size = heap_size - 1bv4;
	// // minheapify
	// // make sure the index is zero
	// state = MINHEAPIFY;
	// caller_state = BUILD_HUFFMAN;
	// sub_state = EXTRACT_MIN_AS_RIGHT;
	
	// invariant test_clk11_state: ((clk_counter == 11) ==> (state == MINHEAPIFY));
	// invariant test_clk11_caller_state: ((clk_counter == 11) ==> (caller_state == BUILD_HUFFMAN));
	// invariant test_clk11_sub_state: ((clk_counter == 11) ==> (sub_state == EXTRACT_MIN_AS_RIGHT));
	// invariant test_clk11_index: ((clk_counter == 11) ==> (heap_index == 0bv4));
	// invariant test_clk11_size: ((clk_counter == 11) ==> (heap_size == 2bv4));
	// invariant test_clk11_swap: ((clk_counter == 11) ==> (heap_array[0bv4] == 1bv4));
	// invariant test_clk11_left: ((clk_counter == 11) ==> (w_freq == 1bv6 && w_left_child_addr == 3bv4));
	// smallest_heap_index
	// invariant test_clk11_sml_index: ((clk_counter == 11) ==> (smallest_heap_index == 1bv4));

	// minheapify on index 0
	// heap_array[smallest_heap_index] = heap_array[heap_index];
	// // heap_array[heap_index] = heap_array[smallest_heap_index];
	// heap_array[heap_index] = tmp_addr;
	// // update heap index for recursive call
	// heap_index = smallest_heap_index;
	// invariant test_clk12_index: ((clk_counter == 12) ==> (heap_index == 1bv4));

	// back to extract right
	// invariant test_clk13_index: ((clk_counter == 13) ==> (heap_index == 0bv4));
	// invariant test_clk13_state: ((clk_counter == 13) ==> (state == BUILD_HUFFMAN));

	// extracted right
	// w_freq = w_freq + freq_mem[r_node_addr_1];
	// w_right_child_addr = heap_array[heap_index];
	// // swap
	// // heap_index shall be zero
	// heap_array[heap_index] = new_node_addr_counter;
	// // write to node memory
	// // TODO make it internal
	// valid = true;
	// w_node_addr = new_node_addr_counter;
	// new_node_addr_counter = new_node_addr_counter + 1bv4;
	// // delay minheapify
	// // such that the write can finishe first
	// invariant test_clk14_state: ((clk_counter == 14) ==> (state == BUILD_HUFFMAN));
	// invariant test_clk14_sub_state: ((clk_counter == 14) ==> (sub_state == NEW_NODE_MINHEAPIFY));
	// invariant test_clk14_index: ((clk_counter == 14) ==> (heap_index == 0bv4));
	// invariant test_clk14_size: ((clk_counter == 14) ==> (heap_size == 2bv4));
	// invariant test_clk14_swap: ((clk_counter == 14) ==> (heap_array[0bv4] == 9bv4));
	// invariant test_clk14_right: ((clk_counter == 14) ==> (w_freq == 3bv6 && w_right_child_addr == 2bv4));

	// writed to mem and to minheapify
	// valid = false;
	// // minheapify
	// state = MINHEAPIFY;
	// caller_state = BUILD_HUFFMAN;
	// // looooooooooooop
	// // it stops or not depending on the heap_size
	// sub_state = EXTRACT_MIN_AS_LEFT;
	// invariant test_clk15_sml_index: ((clk_counter == 15) ==> (smallest_heap_index == 0bv4));
	
	// back to build
	// invariant test_clk16_state: ((clk_counter == 16) ==> (state == BUILD_HUFFMAN));
	// invariant test_clk16_sub_state: ((clk_counter == 16) ==> (sub_state == EXTRACT_MIN_AS_LEFT));

	// clk 17 extracted left, and go to miheapyfy
	// clk 18 mihepfy: go back to extract right
	// clk 19 extracted right
	// clk 20 write to mem and to minheapify
	// invariant test_clk20_state: ((clk_counter == 20) ==> (state == MINHEAPIFY));
	// invariant test_clk20_sml_index: ((clk_counter == 20) ==> (smallest_heap_index == 0bv4));

	// clk 21 minheapify back to extract left and find out size == 1
	// invariant test_clk21_state: ((clk_counter == 21) ==> (state == BUILD_HUFFMAN));
	// invariant test_clk21_size: ((clk_counter == 21) ==> (heap_size == 1bv4));

	// clk 22 done!
	// state = IDLE;
	// done = true;
	// huffman_root = heap_array[0bv4];
	// invariant test_clk22_state: ((clk_counter == 22) ==> (state == IDLE));
	// invariant test_clk22_done: ((clk_counter == 22) ==> (done == true));
	// invariant test_clk22_root: ((clk_counter == 22) ==> (huffman_root == 10bv4));

	// property[LTL] eventually_minheapify: F(state == MINHEAPIFY);
	// property[LTL] always_read: G(state == READ_ALPHABET);
	// property[LTL] eventually_set_heap_size: F(heap_size == 2bv4);
	
	// abstractly...

	// invariant extracted_is_smallest: 
	// invariant 
	
	// property[LTL] eventually_done: F(done == true);

	// assume (ALPHABET_START_ADDR == 1bv4);
	// assume (ALPHABET_SIZE == 8bv4);

	// test abstract
	// invariant state_inv: (false);

	control {
		// 134
		vobj = bmc_noLTL(40);
		// or vobj = induction;
		check;
		// print_results;
		vobj.print_cex(); // list of param p35
	}
}