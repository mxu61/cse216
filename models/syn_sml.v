module syn_sml {
    var p, l, r: integer;
    var ls1, ls2, ls3: boolean;

    init {
        havoc p;
        havoc l;
        havoc r;

        ls1 = p > l;
        ls2 = r > p;
        ls3 = r > l;
    }

    next {
    }

// synthesis function
synthesis function
    get_smallest(eq1: boolean, eq2: boolean, eq3: boolean,
                val1: integer, val2: integer, val3: integer): integer;
// returen value is one of the three
invariant assert_inside: (get_smallest(ls1, ls2, ls3, p, l, r) == p ||
                        get_smallest(ls1, ls2, ls3, p, l, r) == l ||
                        get_smallest(ls1, ls2, ls3, p, l, r) == r);

// actual smallest
invariant assert_smallest: (get_smallest(ls1, ls2, ls3, p, l, r) <= p &&
                            get_smallest(ls1, ls2, ls3, p, l, r) <= l &&
                            get_smallest(ls1, ls2, ls3, p, l, r) <= r);

    control {
        vobj = bmc(1);
        check;
        // vobj.print_cex();
        print_results;
    }
}