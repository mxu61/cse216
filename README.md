#### Usage

1. Install [UCLID5](https://github.com/uclid-org/uclid)
2. To run model verification, go to the `model` directory, run the `run_constructor.sh` script, or use command `uclid common.ucl constructor_clean.v -m constructor`
3. To run function synthesis, go to the `model` directory, run command `uclid syn_sml.v -m syn_sml`

#### Files

- `common.ucl`: type definitions

- `constructor_clean.v` : the latest Huffman constructor file
- `syn_sml.v`: the function synthesis file
- `extractor.ucl`: the initial modelling of the Huffman extractor (untested)

